package com.yshlomo.servicenow.adapters

import android.support.v7.widget.RecyclerView
import android.content.Context
import android.support.v7.widget.CardView
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import com.yshlomo.servicenow.R
import com.yshlomo.servicenow.retrofit.models.ResultPojoModel


class JokesAdapter(context: Context, data: List<ResultPojoModel>, val mPositionListener: PositionListener) :
    RecyclerView.Adapter<JokesAdapter.ViewHolder>() {

    private val TAG = JokesAdapter::class.java.simpleName
    private val items = data
    private val mInflater: LayoutInflater
    private val mContext = context


    init {
        mInflater = LayoutInflater.from(mContext);
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.list_item_layout, parent,false) as CardView
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mPositionListener.onPositionChanged(position)
        holder.textView.setText(items.get(position).value?.joke)
    }

    inner class ViewHolder internal constructor(itemView: CardView) : RecyclerView.ViewHolder(itemView){
        internal var textView: TextView

        init {
            textView = itemView.findViewById(R.id.item_view) as TextView
        }
    }

    interface PositionListener{
        fun onPositionChanged(position: Int)
    }

}