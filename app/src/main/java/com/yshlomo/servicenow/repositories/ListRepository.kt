package com.yshlomo.servicenow.repositories

import android.content.Context
import com.yshlomo.servicenow.retrofit.SearchService
import com.yshlomo.servicenow.retrofit.models.ResultPojoModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor



class ListRepository(val mContext: Context){

    private val mSearchService: SearchService

    init{
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(logging)  // <-- this is the important line!


        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .baseUrl("https://api.icndb.com/")
            .build()

        mSearchService = retrofit.create(SearchService::class.java)
    }

    fun getJokes(firstName: String, lastName: String, limitTo: String): Observable<ResultPojoModel> {
        return mSearchService.getJokes(firstName, lastName, limitTo)

    }
}