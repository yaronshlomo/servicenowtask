package com.yshlomo.servicenow.retrofit.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Value {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("joke")
    @Expose
    var joke: String? = null
    @SerializedName("categories")
    @Expose
    var categories: List<Any>? = null

}
