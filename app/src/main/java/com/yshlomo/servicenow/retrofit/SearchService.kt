package com.yshlomo.servicenow.retrofit

import com.yshlomo.servicenow.retrofit.models.ResultPojoModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface SearchService {

    @GET("jokes/random?/")
    fun getJokes(
        @Query("firstName") firstName: String,
        @Query("lastName") lastName: String,
        @Query("limiTo") limitTo: String):
            Observable<ResultPojoModel>

}