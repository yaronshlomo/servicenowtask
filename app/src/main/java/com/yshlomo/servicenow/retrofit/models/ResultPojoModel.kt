package com.yshlomo.servicenow.retrofit.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResultPojoModel {

    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("value")
    @Expose
    var value: Value? = null
}
