package com.yshlomo.servicenow.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yshlomo.servicenow.R
import com.yshlomo.servicenow.ServiceNowApplication
import com.yshlomo.servicenow.viewmodels.DetailsViewModel
import com.yshlomo.servicenow.viewmodels.JokesViewModel
import kotlinx.android.synthetic.main.items_fragment.*
import javax.inject.Inject

class JokesFragment : Fragment() {

    private val TAG = JokesFragment::class.java.simpleName

    companion object {
        val FIRST_NAME_KEY = "first_name"
        val LAST_NAME_KEY = "last_name_key"
    }

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    private val mJokesViewModel: JokesViewModel by lazy {
        ViewModelProviders.of(this, mViewModelFactory).get(JokesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        (activity!!.application as ServiceNowApplication).getAppComponent().viewBuilder().build().inject(this)
        return inflater.inflate(R.layout.items_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var firstName: String? = null
        var lastName: String? = null

        if(arguments != null){
            firstName = arguments!!.getString(FIRST_NAME_KEY)
            lastName = arguments!!.getString(LAST_NAME_KEY)
        }

        if(!firstName.isNullOrEmpty() && !lastName.isNullOrEmpty() && mJokesViewModel.mSearchAdapter == null){
            mJokesViewModel.getJokes(firstName, lastName,"nerdi")
        }else if(mJokesViewModel.mSearchAdapter != null){
            jokes_list.adapter = mJokesViewModel.mSearchAdapter
            jokes_list.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        }

        mJokesViewModel.mJokesLiveData.observe(this, Observer {
            if(it?.setAdapter!! && it.jokesAdapter != null){
                jokes_list.adapter = it.jokesAdapter
                jokes_list.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            }

            if(it.showError){
                Snackbar.make(view, it.errorMessage, Snackbar.LENGTH_LONG).show()
                it.showError = false
            }

        })

        lifecycle.addObserver(mJokesViewModel)
    }


}