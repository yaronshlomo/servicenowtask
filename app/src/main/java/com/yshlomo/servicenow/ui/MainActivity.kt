package com.yshlomo.servicenow.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import com.yshlomo.servicenow.R
import com.yshlomo.servicenow.models.DisplayResultModel
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import com.yshlomo.servicenow.ServiceNowApplication


class MainActivity : AppCompatActivity(), DetailsFragment.DetailsFragmentListener {
    private val TAG = MainActivity::class.java.simpleName

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    private val mMainScreenViewModel: MainScreenViewModel by lazy {
        ViewModelProviders.of(this, mViewModelFactory).get(MainScreenViewModel::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as ServiceNowApplication).getAppComponent().viewBuilder().build().inject(this)

        setContentView(R.layout.activity_main)

        mMainScreenViewModel.mSearchResultLiveData.observe(this, Observer {
            onDisplayChange(it!!)
        })

        lifecycle.addObserver(mMainScreenViewModel)
    }

    override fun onAttachFragment(fragment: Fragment?) {
        if (fragment is DetailsFragment) {
            val detailsFragment = fragment as DetailsFragment?
            detailsFragment!!.setFragmentListener(this)
        }
    }

    override fun onSubmit(firstName: String, lastName: String) {
        mMainScreenViewModel.onSubmitData(firstName, lastName)
    }

    private fun onDisplayChange(displayResultModel: DisplayResultModel) {
        if (displayResultModel.fragmentType == MainScreenViewModel.FragmentType.JokesFragment) {
            showJokesFragment(displayResultModel)
        }

        if(displayResultModel.fragmentType == MainScreenViewModel.FragmentType.DetailsFragment){
            showDetailFragment()
        }
    }

    private fun showDetailFragment() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()

        movie_detail_container.visibility = View.VISIBLE

        val fragment = DetailsFragment()

        val currentFragment = fragmentManager.findFragmentByTag(DetailsFragment::class.java.simpleName)

        if(currentFragment == null){
            fragmentTransaction?.add(
                R.id.movie_detail_container,
                fragment,
                DetailsFragment::class.java.simpleName
            )
            fragmentTransaction?.commit()
        }

    }

    private fun showJokesFragment(displayResultModel: DisplayResultModel) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()

        movie_detail_container.visibility = View.VISIBLE

        val fragment = JokesFragment()

        fragment.arguments = displayResultModel.fragmentArguments

        val currentFragment = fragmentManager.findFragmentByTag(JokesFragment::class.java.simpleName)

        if(currentFragment == null){
            fragmentTransaction?.replace(
                R.id.movie_detail_container,
                fragment,
                JokesFragment::class.java.simpleName
            )
            fragmentTransaction?.commit()
        }

    }
}
