package com.yshlomo.servicenow.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.jakewharton.rxbinding2.widget.RxTextView
import com.yshlomo.servicenow.R
import com.yshlomo.servicenow.ServiceNowApplication
import com.yshlomo.servicenow.models.DetailsModel
import com.yshlomo.servicenow.viewmodels.DetailsViewModel
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.details_fragment.*
import javax.inject.Inject




class DetailsFragment : Fragment() {

    private val TAG = DetailsFragment::class.java.simpleName

    private var mDetailsFragmentListener: DetailsFragmentListener? = null
    private lateinit var mFirstNameObservable: Observable<Unit>
    private lateinit var mLastNameObservable: Observable<Unit>

    private lateinit var mFirstNameDisposable: Disposable
    private lateinit var mLastNameDisposable: Disposable

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    private val mDetailsViewModel: DetailsViewModel by lazy {
        ViewModelProviders.of(this, mViewModelFactory).get(DetailsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        (activity!!.application as ServiceNowApplication).getAppComponent().viewBuilder().build().inject(this)
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mFirstNameObservable = RxTextView.textChanges(first_name)
            .map { searchText -> mDetailsViewModel.onFirstNameChanged(searchText.toString()) }

        mLastNameObservable = RxTextView.textChanges(last_name)
            .map { searchText -> mDetailsViewModel.onLastNameChanged(searchText.toString()) }

        submit_button.setOnClickListener {
            mDetailsFragmentListener!!.onSubmit(
                mDetailsViewModel.mFirstName,
                mDetailsViewModel.mLastName
            )
        }

        mDetailsViewModel.mJokesLiveData.observe(this, Observer { onDisplayChanged(it!!) })

        lifecycle.addObserver(mDetailsViewModel)

    }

    override fun onResume() {
        super.onResume()

        mFirstNameDisposable = mFirstNameObservable.subscribe()
        mLastNameDisposable = mLastNameObservable.subscribe()

    }

    override fun onPause() {
        super.onPause()
        mFirstNameDisposable.dispose()
        mLastNameDisposable.dispose()
    }

    private fun onDisplayChanged(detailsModel: DetailsModel) {
        submit_button.isEnabled = detailsModel.submitEnable

        first_name_counter.setText(detailsModel.firstNameConter)
        last_name_counter.setText(detailsModel.lastNameConter)

        if(detailsModel.showError){
            Snackbar.make(view!!, detailsModel.errorMessage, Snackbar.LENGTH_LONG).show()
            detailsModel.showError = false
        }
    }

    fun setFragmentListener(detailsFragmentListener: DetailsFragmentListener) {
        mDetailsFragmentListener = detailsFragmentListener
    }

    interface DetailsFragmentListener {
        fun onSubmit(firstName: String, lastName: String)
    }
}