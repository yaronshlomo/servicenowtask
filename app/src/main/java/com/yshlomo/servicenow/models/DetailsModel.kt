package com.yshlomo.servicenow.models

data class DetailsModel(var firstNameLength: Int = 0,
                        var lastNameLength: Int = 0,
                        var firstNameConter: String = "",
                        var lastNameConter: String = "",
                        var submitEnable: Boolean = true):BaseModel()