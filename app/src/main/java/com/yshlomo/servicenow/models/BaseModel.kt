package com.yshlomo.servicenow.models

open class BaseModel(  var showError: Boolean = false,
                       var errorMessage: String = "")