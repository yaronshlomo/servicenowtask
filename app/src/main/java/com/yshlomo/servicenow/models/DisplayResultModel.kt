package com.yshlomo.servicenow.models

import android.os.Bundle
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel

class DisplayResultModel(var closeKeyboard: Boolean = false,
                         var fragmentType: MainScreenViewModel.FragmentType = MainScreenViewModel.FragmentType.DetailsFragment,
                         var fragmentArguments: Bundle? = null)
