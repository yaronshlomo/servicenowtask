package com.yshlomo.servicenow.models

import com.yshlomo.servicenow.adapters.JokesAdapter

data class JokesListModel(var jokesAdapter: JokesAdapter? = null,
                          var setAdapter: Boolean = false): BaseModel()