/*
 * code:<br />Copyright (C) 2018 SIRIN LABS AG
 */

package com.yshlomo.servicenow.injection.modules

import android.arch.lifecycle.ViewModelProvider
import com.yshlomo.servicenow.injection.ServiceNowViewModelFactory
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Configures bindings to [WalletViewModelFactory], injectable into a [ViewModelProvider.Factory].
 */
//@Singleton
@Module(includes = arrayOf(RepositoriesModule::class))
abstract class ViewModelInjectionModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ServiceNowViewModelFactory): ViewModelProvider.Factory
}