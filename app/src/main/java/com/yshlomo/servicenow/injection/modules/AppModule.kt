package com.yshlomo.servicenow.injection.modules

import android.app.Application
import android.content.Context
import com.yshlomo.servicenow.injection.components.RepositoriesComponent
import com.yshlomo.servicenow.injection.components.ViewsComponent
import dagger.Module
import dagger.Provides


@Module(subcomponents = arrayOf(ViewsComponent::class, RepositoriesComponent::class))
class AppModule(application : Application) {

    private val mApplication : Application = application
    private val appContext : Context = mApplication.baseContext

    @Provides
    fun provideApp() : Application = mApplication

    @Provides
    fun provideContext() = appContext
}
