package com.yshlomo.servicenow.injection.components

import com.yshlomo.servicenow.injection.RepositoriesScope
import com.yshlomo.servicenow.injection.modules.RepositoriesModule
import dagger.Subcomponent

@RepositoriesScope
@Subcomponent(modules = arrayOf(RepositoriesModule::class))
interface RepositoriesComponent {

    @Subcomponent.Builder
    interface Builder {
        fun repositoriesModule(module: RepositoriesModule) : Builder
        fun build(): RepositoriesComponent
    }
}
