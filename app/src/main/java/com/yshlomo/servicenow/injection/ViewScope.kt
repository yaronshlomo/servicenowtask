/*
 * code:<br />Copyright (C) 2018 SIRIN LABS AG
 */

package com.yshlomo.servicenow.injection

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ViewScope