package com.yshlomo.servicenow.injection.components

import com.yshlomo.servicenow.injection.ViewScope
import com.yshlomo.servicenow.injection.modules.ViewModelInjectionModule
import com.yshlomo.servicenow.ui.DetailsFragment
import com.yshlomo.servicenow.ui.JokesFragment
import com.yshlomo.servicenow.ui.MainActivity
import dagger.Subcomponent
import javax.inject.Scope
import javax.inject.Singleton

/*@ViewScope*/
@Singleton
@Subcomponent(modules = arrayOf(ViewModelInjectionModule::class))
interface ViewsComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(detailsFragment: DetailsFragment)
    fun inject(jokesFragment: JokesFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): ViewsComponent
    }
}
