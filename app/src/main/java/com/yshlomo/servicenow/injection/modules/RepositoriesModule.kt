package com.yshlomo.servicenow.injection.modules

import android.content.Context
import com.yshlomo.servicenow.repositories.ListRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

//@Singleton
@Module
class RepositoriesModule {

    @Provides
    @Reusable
    fun provideListRepository(context: Context) = ListRepository(context)

}
