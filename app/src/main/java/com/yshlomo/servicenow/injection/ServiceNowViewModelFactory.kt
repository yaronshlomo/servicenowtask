/*
 * code:<br />Copyright (C) 2018 SIRIN LABS AG
 */

package com.yshlomo.servicenow.injection

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.yshlomo.servicenow.repositories.ListRepository
import com.yshlomo.servicenow.viewmodels.DetailsViewModel
import com.yshlomo.servicenow.viewmodels.JokesViewModel
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceNowViewModelFactory @Inject constructor(private val mApplication: Application,
                                                     private val mListRepository: ListRepository,
                                                     private val mContext: Context) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(MainScreenViewModel::class.java) -> MainScreenViewModel(mContext)
                    isAssignableFrom(DetailsViewModel::class.java) -> DetailsViewModel(mContext)
                    isAssignableFrom(JokesViewModel::class.java) -> JokesViewModel(mContext, mListRepository)

                    else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T
}