package com.yshlomo.servicenow.injection.components

import com.yshlomo.servicenow.injection.modules.AppModule
import dagger.Component
import javax.inject.Singleton

//@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun viewBuilder(): ViewsComponent.Builder
    fun repositoriesBuilder(): RepositoriesComponent.Builder
}