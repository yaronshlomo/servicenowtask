package com.yshlomo.servicenow

import android.app.Application
import com.yshlomo.servicenow.injection.components.AppComponent
import com.yshlomo.servicenow.injection.modules.AppModule
import com.yshlomo.servicenow.injection.components.DaggerAppComponent

class ServiceNowApplication: Application(){

    lateinit var mComponent : AppComponent

    fun getAppComponent() : AppComponent = mComponent

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger() {
        mComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()

        mComponent.repositoriesBuilder().build()
    }


}