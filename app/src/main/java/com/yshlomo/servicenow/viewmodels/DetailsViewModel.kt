package com.yshlomo.servicenow.viewmodels

import android.arch.lifecycle.*
import android.content.Context
import com.yshlomo.servicenow.R
import com.yshlomo.servicenow.models.DetailsModel

class DetailsViewModel(val mContext: Context): ViewModel(), LifecycleObserver {

    lateinit var mFirstName: String
    lateinit var mLastName: String

    var mDetailsModel = DetailsModel()
    lateinit var mCounterFormat: String

    val mJokesLiveData: MediatorLiveData<DetailsModel> = MediatorLiveData()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(){
        enableSubmitButton()
        mCounterFormat = mContext.getString(R.string.counter_format)
    }

    fun onFirstNameChanged(firstName: String) {
        mFirstName = firstName
        mDetailsModel.firstNameLength = firstName.length
        mDetailsModel.firstNameConter = String.format(mCounterFormat, mDetailsModel.firstNameLength)
        if( mDetailsModel.firstNameLength >= 20){
            mDetailsModel.showError = true
            mDetailsModel.errorMessage = mContext.getString(R.string.max_length_error)
        }
        enableSubmitButton()
        mJokesLiveData.postValue(mDetailsModel)
    }

    fun onLastNameChanged(lastName: String) {
        mLastName = lastName
        mDetailsModel.lastNameLength = lastName.length
        mDetailsModel.lastNameConter = String.format(mCounterFormat, mDetailsModel.lastNameLength)
        if( mDetailsModel.lastNameLength >= 20){
            mDetailsModel.showError = true
            mDetailsModel.errorMessage = mContext.getString(R.string.max_length_error)
        }
        enableSubmitButton()
        mJokesLiveData.postValue(mDetailsModel)
    }

    private fun enableSubmitButton(){
        mDetailsModel.submitEnable = mDetailsModel.firstNameLength > 0 && mDetailsModel.lastNameLength > 0
    }

}