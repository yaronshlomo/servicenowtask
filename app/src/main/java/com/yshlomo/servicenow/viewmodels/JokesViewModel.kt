package com.yshlomo.servicenow.viewmodels

import android.arch.lifecycle.*
import android.content.Context
import android.util.Log
import com.yshlomo.servicenow.adapters.JokesAdapter
import com.yshlomo.servicenow.models.JokesListModel
import com.yshlomo.servicenow.repositories.ListRepository
import com.yshlomo.servicenow.retrofit.models.ResultPojoModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class JokesViewModel(val mContext: Context, var mRepository: ListRepository) : ViewModel(), LifecycleObserver,
    JokesAdapter.PositionListener {

    val mJokesLiveData: MediatorLiveData<JokesListModel> = MediatorLiveData()
    var mSearchAdapter: JokesAdapter? = null
    private val mListItems = mutableListOf<ResultPojoModel>()
    private lateinit var mLastName: String
    private lateinit var mFirstName: String
    private lateinit var mLimitTo: String

    private var TAG  = JokesViewModel::class.java.simpleName

    private lateinit var mDisplayResultModel: JokesListModel

    fun onResult(searchResult: ResultPojoModel) {
        mDisplayResultModel = JokesListModel()
        mListItems.add(searchResult)
        if(mSearchAdapter == null) {
            mSearchAdapter = JokesAdapter(mContext, mListItems, this)
            mDisplayResultModel.jokesAdapter = mSearchAdapter
            mDisplayResultModel.setAdapter = true
        }else{
            mSearchAdapter?.notifyItemInserted(mListItems.size)
        }
        mJokesLiveData.postValue(mDisplayResultModel)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(){

    }

    fun getJokes(firstName: String, lastName:String, limitTo: String){
        mLastName = lastName
        mFirstName = firstName
        mLimitTo = limitTo

        mRepository.getJokes(firstName, lastName, limitTo).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({onResult(it)}, {throwable->Log.e(TAG, throwable.localizedMessage)})
    }

    override fun onPositionChanged(position: Int) {
      if(mListItems.size == position + 1){
          getJokes(mFirstName, mLastName, mLimitTo)
      }
    }

}