package com.yshlomo.servicenow.viewmodels

import android.arch.lifecycle.*
import android.content.Context
import android.os.Bundle
import com.yshlomo.servicenow.models.DisplayResultModel
import com.yshlomo.servicenow.ui.JokesFragment.Companion.FIRST_NAME_KEY
import com.yshlomo.servicenow.ui.JokesFragment.Companion.LAST_NAME_KEY


class MainScreenViewModel(context: Context): ViewModel(), LifecycleObserver {

    private val TAG = MainScreenViewModel::class.java.simpleName

    enum class FragmentType{
        DetailsFragment,
        JokesFragment
    }

    private val mContext = context
    val mSearchResultLiveData: MediatorLiveData<DisplayResultModel> = MediatorLiveData()

    private var mDisplayResultModel = DisplayResultModel()

    fun onSubmitData(firstName: String, lastName: String){
        var bundle = Bundle()
        bundle.putString(FIRST_NAME_KEY, firstName)
        bundle.putString(LAST_NAME_KEY, lastName)
        mDisplayResultModel.fragmentArguments = bundle
        mDisplayResultModel.fragmentType = FragmentType.JokesFragment
        mSearchResultLiveData.postValue(mDisplayResultModel)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(){
        mSearchResultLiveData.postValue(mDisplayResultModel)
    }

}